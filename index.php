<?php

class Autor {
private $imie;
private $nazwisko;

public function __construct($imie, $nazwisko) {
$this->imie = $imie;
$this->nazwisko = $nazwisko;
}

public function getImie() {
return $this->imie;
}

public function getNazwisko() {
return $this->nazwisko;
}
}

class Pytanie {
private $autor;
private $pytanie;

public function __construct($pytanie, Autor $autor) {
$this->autor = $autor;
$this->pytanie = $pytanie;
}

public function getAutor() {
return $this->autor;
}

public function getPytanie() {
return $this->pytanie;
}
}